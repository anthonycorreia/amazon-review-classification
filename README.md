# Amazon review classification

To clone the repository
```bash
git clone git@gitlab.com:anthonycorreia/amazon-review-classification.git
```

## Setup

### Conda environment

You need [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)
to set up the environment.
You may want to use the much faster
[mamba](https://github.com/conda-forge/miniforge#mambaforge) alternative.

You can import the environment using
```bash
mamba env create --file environment.yaml
```
or, with conda
```bash
conda env create --file environment.yaml
```

Then activate it using
```bash
conda activate amazon_classification
```

You can activate the widgets in the jupyter notebooks using the following command
```bash
jupyter nbextension enable --py widgetsnbextension
```


### Word embedding

The word embedding need to be downloaded and extracted in the `glove/` folder.

```bash
mkdir glove
wget https://nlp.stanford.edu/data/glove.6B.zip -P glove
unzip glove/glove.6B.zip -d glove
```

## Notebooks

The notebooks are located in `notebooks/`.


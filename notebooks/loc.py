"""Configurate paths of input and output files
"""
import os.path as op

# List of set types
settypes = [
    "dev",
    "test",
    "train"
]

#: Name of the tensorflow dataset used in this analysis
tf_dataset_name = 'amazon_us_reviews/Wireless_v1_00'

#: Path to the folder where tensorflow data is saved
data_dir = "../data/"

#: Path where the tensorflow datasets are saved
tf_data_dir = op.join(data_dir, "tf_datasets")

#: Path where pre-processed tensorflow data is saved (with placeholders)
wildcard_preprocessed_path = op.join(
    data_dir,
    "preprocessed",
    "{dev}_{test}_{train}_seed{seed}",
    "{settype}"
)

#: Path to the folder where glove word to vector maps are saved
glove_dir = "../glove/"

#: Path to the glove file used in this analysis
glove_path = op.join(glove_dir, "glove.6B.100d.txt")

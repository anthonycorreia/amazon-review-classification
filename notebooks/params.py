"""Definition of the parameters used in this analysis.
"""

#: Associates a set type with its # entries
cardinalities = {
    "dev": 10000,
    "test": 10000,
    "train": 200000
}

#: Seed for whatever shuffling
seed = 1

#: Number of possible ratings
nb_rating = 5
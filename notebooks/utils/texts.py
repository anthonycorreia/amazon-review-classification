"""Helpers to deal with tensorflow text.
"""
from typing import Dict
import tensorflow as tf

def regex_replace_all(
    string: tf.Tensor,
    replace: Dict[str, str]
) -> tf.Tensor:
    """Apply `tf.strings.regex_replace` several times.
    
    Args:
        string: string to process
        replace: a dictionnary that associates a pattern with what to replace it
            with
    
    Returns:
        Processed string
    """
    for key, value in replace.items():
        string = tf.strings.regex_replace(
            string,
            key,
            value,
            replace_global=True
        )
    return string

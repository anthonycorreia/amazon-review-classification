"""Helpers to deal with Tensorflow datasets
"""
from typing import List
from tensorflow.data import Dataset

def split_dataset(
    dataset: Dataset,
    counts: List[int]
) -> List[Dataset]:
    """Split a dataset given the number of entries
    required for each dataset.
    
    Args:
        dataset: dataset to split
        counts: numbers of entries expected in each
            dataset. The size of this list corresponds
            to the number of datasets.
            
    Returns:
        list of datasets whose cardinalities are given
        by ``counts``, or less if there is not enough
        entries in ``dataset``.
    """
    datasets = []

    running_dataset = dataset
    for previous_count, count in zip([0] + counts[:-1], counts):
        running_dataset = running_dataset.skip(previous_count)
        datasets.append(running_dataset.take(count))
             
    return datasets



def concatenate_datasets(
    datasets: List[Dataset]
) -> Dataset:
    """Concatenate datasets (deterministic)
    
    Args:
        datasets: list of datasets
    
    Returns:
        concatenated dataset
    """
    concatenated_dataset = datasets[0]
    for sub_dataset in datasets[1:]:
        concatenated_dataset = concatenated_dataset.concatenate(
            sub_dataset
        )
    return concatenated_dataset


def compute_cardinality(dataset: Dataset) -> int:
    """Count the number of element in a dataset.
    
    Args:
        dataset: a dataset
    
    Returns:
        number of entries in this dataset
    """
    return sum(1 for _ in dataset)
